import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/shared/authentication.service';
import { Menu, MenuItemService } from 'src/app/shared/models/menu-item';
import { UtilisateurJwt } from 'src/app/shared/models/utilisateur.model';

@Component({
  selector: 'app-sire-bar',
  templateUrl: './sire-bar.component.html',
  styleUrls: ['./sire-bar.component.scss']
})
export class SireBarComponent implements OnInit {

  menu:Array<Menu> = [];

  constructor(private menuItemService:MenuItemService) { }

  ngOnInit() {
    this.menu = this.menuItemService.getAll();
    console.table(this.menu)
  }


}
