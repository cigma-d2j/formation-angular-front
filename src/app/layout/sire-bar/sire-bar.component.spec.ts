import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SireBarComponent } from './sire-bar.component';

describe('SireBarComponent', () => {
  let component: SireBarComponent;
  let fixture: ComponentFixture<SireBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SireBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SireBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
