import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticleComponent } from './components/article/article.component';
import { CategorieComponent } from './components/categorie/categorie.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ErrorComponent } from './components/error/error.component';
import { GetsionUtilisateurComponent } from './components/getsion-utilisateur/getsion-utilisateur.component';
import { InscriptionComponent } from './components/inscription/inscription.component';
import { LoginComponent } from './components/login/login.component';
import { PanierComponent } from './components/panier/panier.component';


const routes: Routes = [
  {
    path : '',
    redirectTo : 'dashboard',
    pathMatch : 'full'
  },
  {
    path : 'dashboard',
    component : DashboardComponent
  },
  {
    path : 'article',
    component: ArticleComponent
  },
  {
    path : 'categorie',
    component: CategorieComponent
  },
  {
    path : 'panier',
    component: PanierComponent
  },
  {
    path : 'gestionutilisateur',
    component: GetsionUtilisateurComponent
  },
  {
    path : 'login',
    component: LoginComponent
  },
  {
    path : 'inscritpion',
    component: InscriptionComponent
  },
  {
    path : '**',
    component: ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
