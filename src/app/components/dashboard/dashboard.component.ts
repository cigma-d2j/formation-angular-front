import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  redirectToArticlePage(){
    this.router.navigate(['article']).then(element => {
      element === true ? alert('Congratulation you are in the article page') : alert("Oops, this page does not existe any more, contact the technical team :p")
    })
  }

}
