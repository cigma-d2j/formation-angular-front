import { Component, OnInit } from '@angular/core';
import { GestionUtilisateurService } from 'src/app/shared/services/gestion-utilisateur.service';

@Component({
  selector: 'app-getsion-utilisateur',
  templateUrl: './getsion-utilisateur.component.html',
  styleUrls: ['./getsion-utilisateur.component.scss']
})
export class GetsionUtilisateurComponent implements OnInit {

  constructor(private gestionUserService:GestionUtilisateurService) { }

  ngOnInit() {

    this.gestionUserService.getAllUser().subscribe(response => console.table(response));
  }

}
