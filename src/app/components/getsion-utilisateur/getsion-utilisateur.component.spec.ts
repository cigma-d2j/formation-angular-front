import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetsionUtilisateurComponent } from './getsion-utilisateur.component';

describe('GetsionUtilisateurComponent', () => {
  let component: GetsionUtilisateurComponent;
  let fixture: ComponentFixture<GetsionUtilisateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetsionUtilisateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetsionUtilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
