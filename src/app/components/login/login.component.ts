import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/shared/authentication.service';
import { HelperService } from 'src/app/shared/helper/helper.service';
import { UtilisateurJwt } from 'src/app/shared/models/utilisateur.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;

  constructor(private authenticationService:AuthenticationService,
              private helper:HelperService) { }

  ngOnInit() {
  }

  authneticate(){
    let utilistaeurJwt: UtilisateurJwt = new UtilisateurJwt(this.username,this.password);
    this.authenticationService.getJwtToken(utilistaeurJwt).subscribe(response => {
      this.helper.saveJwttoLocalStaorage(response)
    });
  }
}
