import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/shared/authentication.service';
import { UtilisateurBack } from 'src/app/shared/models/utilisateur-back.model';
import { MustMatch } from 'src/app/shared/validators/checkPassword.validators';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {

  formGroupRegister:FormGroup

  constructor(private formBuilder:FormBuilder, private authenticationService:AuthenticationService) {
    this.formGroupRegister = this.formBuilder.group({
      nom : [null, [Validators.minLength(3), Validators.required]],
      prenom : [null, [Validators.maxLength(16), Validators.minLength(3), Validators.required]],
      username : [null, [Validators.minLength(3), Validators.required]],
      cin : [null, [Validators.minLength(3), Validators.required]],
      telephone : [null, [Validators.minLength(3), Validators.required]],
      adresse : [null, [Validators.minLength(3), Validators.required]],
      password : [null, [Validators.minLength(8), Validators.required, Validators.maxLength(16)]],
      passwordMach :  [null, [Validators.minLength(8), Validators.required, Validators.maxLength(16)]],
    }, {validators : MustMatch('password','passwordMach')})

  }

  ngOnInit() {
    console.log(this.formGroupRegister)

    this.formGroupRegister.valueChanges.subscribe(element => {
      console.log(this.formGroupRegister)
    })
  }

  register(){
    let utilisaterBack = new UtilisateurBack(
      this.formGroupRegister.controls['nom'].value,
      this.formGroupRegister.controls['prenom'].value,
      this.formGroupRegister.controls['username'].value,
      this.formGroupRegister.controls['password'].value,
      this.formGroupRegister.controls['cin'].value,
      this.formGroupRegister.controls['telephone'].value,
      this.formGroupRegister.controls['adresse'].value);

    this.authenticationService.inscriptionUser(utilisaterBack).subscribe(reponse => {
      alert("Utilistaeur enregisterer")
    })
  }

}
