import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './layout/nav-bar/nav-bar.component';
import { SireBarComponent } from './layout/sire-bar/sire-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ArticleComponent } from './components/article/article.component';
import { CategorieComponent } from './components/categorie/categorie.component';
import { PanierComponent } from './components/panier/panier.component';
import { GetsionUtilisateurComponent } from './components/getsion-utilisateur/getsion-utilisateur.component';
import { LoginComponent } from './components/login/login.component';
import { InscriptionComponent } from './components/inscription/inscription.component';
import { ErrorComponent } from './components/error/error.component'

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    SireBarComponent,
    DashboardComponent,
    ArticleComponent,
    CategorieComponent,
    PanierComponent,
    GetsionUtilisateurComponent,
    LoginComponent,
    InscriptionComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
