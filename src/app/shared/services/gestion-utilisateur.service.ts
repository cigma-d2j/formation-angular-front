import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GestionUtilisateurService {

  private backUri = environment.backUrl + 'api/v1/gestionUtilisateur'

  constructor(private http:HttpClient) { }

  getAllUser(){
    return this.http.get(this.backUri)
  }
}
