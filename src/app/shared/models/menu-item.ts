import { Injectable } from '@angular/core';


const MENUITEM = [
    {
        label:'Home',
        linkToIt: 'dashboard'
    },
    {
        label:'Articles',
        linkToIt: 'article'
    },
    {
        label:'Categories',
        linkToIt: 'categorie'
    },
    {
        label:'Mon Panier',
        linkToIt: 'panier'
    },
    {
        label:'Gestion d\'Utilisateur',
        linkToIt: 'gestionutilisateur'
    },
    {
        label:'Login',
        linkToIt: 'login'
    },
    {
        label:'Inscription',
        linkToIt: 'inscritpion'
    }
]

export interface Menu{
    label:string,
    linkToIt:string
}

@Injectable({
    providedIn: 'root'
})
export class MenuItemService{
    getAll():Array<Menu>{
        return MENUITEM;
    }
}