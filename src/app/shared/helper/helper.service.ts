import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(private router:Router) { }

  saveJwttoLocalStaorage(jwt:string){
    localStorage.setItem('jwt',jwt)
    let decodedJwt = jwt_decode(jwt);
    localStorage.setItem('roles', JSON.stringify(decodedJwt.roles))
    decodedJwt.roles.forEach(element => {
      if(element.authority === 'Admin'){
        this.router.navigate(["/"])
      }else{
        this.router.navigate(["/article"])
      }
    });
  }


}
