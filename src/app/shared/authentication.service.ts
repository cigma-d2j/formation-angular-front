import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UtilisateurBack } from './models/utilisateur-back.model';
import { UtilisateurJwt } from './models/utilisateur.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  backUri = environment.backUrl;

  constructor(private http:HttpClient) { }

  public getJwtToken(utilisateur:UtilisateurJwt){
    return this.http.post(this.backUri + "login",utilisateur, {responseType : 'text'});
  }

  public inscriptionUser(utilisateurBackEnd:UtilisateurBack){
    //return this.http.get(this.backUri + "producer", {headers: new HttpHeaders({'Authorization':jwtToken})})
    return this.http.post(this.backUri + 'inscription', utilisateurBackEnd)
  }
}
